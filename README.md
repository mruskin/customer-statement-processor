#  Customer Statement Processor
Company receives monthly deliveries of customer statement records. This information is delivered in two formats, CSV and XML. These records need to be validated.

## Input
The format of the file is a simplified format of the MT940 format. The format is as follows:

Field  |Description
----|----
Transaction reference  | A numeric value
Account number   | An IBAN 
Account | IBAN 
Start Balance | The starting balance in Euros 
Mutation | Either and addition (+) or a deducation (-) 
Description | Free text 
End Balance | The end balance in Euros 

## Expected output
There are two validations:
* all transaction references should be unique
* the end balance needs to be validated

At the end of the processing, a report needs to be created which will display both the transaction reference and description of each of the failed records.

## How to test

For business logic testing there are Unit Tests, which cover the main validation flows. (com.rabobank.assignment.unit.AssignmentApplicationTests)
To test the file parsing part there are Rest-Assured integration tests. (com.rabobank.assignment.integration.CustomerStatementControllerIntegrationTest)
