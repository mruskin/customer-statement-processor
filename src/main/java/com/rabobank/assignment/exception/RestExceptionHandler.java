package com.rabobank.assignment.exception;

import com.rabobank.assignment.controller.CustomerStatementController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;

@ControllerAdvice(assignableTypes = {CustomerStatementController.class})
public class RestExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(RestExceptionHandler.class);
    private static final Logger LOG_EMPTY_FILE = LoggerFactory.getLogger(FileFormatException.class);

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleException(Exception ex) {
        LOG.error(ex.getLocalizedMessage(), ex);
        return buildResponseEntity(new ApiError(HttpStatus.NOT_FOUND, "Something went wrong"));
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Object> handleConstraintException(ConstraintViolationException ex) {
        LOG.error(ex.getLocalizedMessage(), ex);
        return buildResponseEntity(new ApiError(HttpStatus.BAD_REQUEST, "Validation failed"));
    }

    @ExceptionHandler(FileFormatException.class)
    public ResponseEntity<Object> handleStockNotFoundException(FileFormatException ex) {
        LOG_EMPTY_FILE.error(ex.getLocalizedMessage());
        return buildResponseEntity(new ApiError(HttpStatus.NOT_FOUND, ex.getMessage()));
    }

    private ResponseEntity<Object> buildResponseEntity(ApiError apiError) {
        return new ResponseEntity<>(apiError, apiError.getStatus());
    }

}
