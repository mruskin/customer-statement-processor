package com.rabobank.assignment.model;

import com.rabobank.assignment.exception.ApiError;

import java.util.List;

public class ApiResponse {

    private ApiError apiError;
    private List<Transaction> transaction;

    public ApiError getApiError() {
        return apiError;
    }

    public void setApiError(ApiError apiError) {
        this.apiError = apiError;
    }

    public List<Transaction> getTransaction() {
        return transaction;
    }

    public void setTransaction(List<Transaction> transaction) {
        this.transaction = transaction;
    }
}
