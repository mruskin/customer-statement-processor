package com.rabobank.assignment.controller;

import com.rabobank.assignment.config.Constants;
import com.rabobank.assignment.config.FileParser;
import com.rabobank.assignment.exception.ApiError;
import com.rabobank.assignment.exception.FileFormatException;
import com.rabobank.assignment.model.ApiResponse;
import com.rabobank.assignment.model.Record;
import com.rabobank.assignment.model.Transaction;
import com.rabobank.assignment.service.ValidatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class CustomerStatementController {

    @Autowired
    private ValidatorService validatorService;

    @Autowired
    private FileParser fileParser;

    @PostMapping("/processStatement")
    @ResponseBody
    public ApiResponse handleFileUpload(@RequestParam("file") MultipartFile file) {
        ApiResponse apiResponse = new ApiResponse();
        if ((file == null) || file.isEmpty()) {
            throw new FileFormatException("The customer statement is null or empty");
        }
        if (!file.getOriginalFilename().contains(Constants.CONTENT_TYPE_TEXT_CSV) && !file.getOriginalFilename().contains(Constants.CONTENT_TYPE_TEXT_XML)) {
            apiResponse.setApiError(new ApiError(HttpStatus.BAD_REQUEST, "Wrong content type"));
            return apiResponse;
        }

        try {
            if (file.getOriginalFilename().contains(Constants.CONTENT_TYPE_TEXT_XML)) {
                List<Transaction> errorTransactions = new ArrayList<>();
                File xmlFile = new File(file.getOriginalFilename());
                file.transferTo(xmlFile);

                validationStep(apiResponse, errorTransactions, fileParser.getTransactionsFromXML(xmlFile));
            } else if (file.getOriginalFilename().contains(Constants.CONTENT_TYPE_TEXT_CSV)) {
                List<Transaction> errorTransactions = new ArrayList<>();
                File csvFile = new File(file.getOriginalFilename());
                file.transferTo(csvFile);
                validationStep(apiResponse, errorTransactions, fileParser.getTransactionsFromCSV(csvFile));
            } else {
                apiResponse.setApiError(new ApiError(HttpStatus.BAD_REQUEST, "Wrong input, or file formats"));
            }
        } catch (Exception ex) {
            throw new FileFormatException("Can't parse the file");
        }
        return apiResponse;
    }

    private void validationStep(ApiResponse apiResponse, List<Transaction> errorTransactions, List<Transaction> transactions) {
        errorTransactions.addAll(validatorService.getDuplicateTransactionReference(transactions));
        errorTransactions.addAll(validatorService.getEndBalanceErrorTransactions(transactions));
        if (!errorTransactions.isEmpty()) {
            apiResponse.setTransaction(errorTransactions);
            apiResponse.setApiError(new ApiError(HttpStatus.OK, Constants.VALIDATION_ERROR));
        } else {
            apiResponse.setApiError(new ApiError(HttpStatus.OK, Constants.VALIDATION_SUCCESS));
        }
    }

}
