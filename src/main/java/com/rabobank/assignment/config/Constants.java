package com.rabobank.assignment.config;

public class Constants {

    public static final String CONTENT_TYPE_APPLICATION_XML = "application/xml";
    public static final String CONTENT_TYPE_TEXT_XML = ".xml";
    public static final String CONTENT_TYPE_TEXT_CSV = ".csv";
    public static final String VALIDATION_SUCCESS = "Validation success!";
    public static final String VALIDATION_ERROR = "Validation error!";
}
