package com.rabobank.assignment.config;

import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.HeaderColumnNameTranslateMappingStrategy;
import com.rabobank.assignment.model.Record;
import com.rabobank.assignment.model.Transaction;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FileParser {

    public List<Transaction> getTransactionsFromCSV(File csvFile) throws FileNotFoundException {
        HeaderColumnNameTranslateMappingStrategy<Transaction> beanStrategy = new HeaderColumnNameTranslateMappingStrategy<>();
        beanStrategy.setType(Transaction.class);

        Map<String, String> columnMapping = new HashMap<>();
        columnMapping.put("Reference", "reference");
        columnMapping.put("AccountNumber", "accountNumber");
        columnMapping.put("Description", "description");
        columnMapping.put("Start Balance", "startBalance");
        columnMapping.put("Mutation", "mutation");
        columnMapping.put("End Balance", "endBalance");

        beanStrategy.setColumnMapping(columnMapping);


        CsvToBean<Transaction> csvToBean = new CsvToBean<>();
        CSVReader reader = new CSVReader(new FileReader(csvFile));
        return csvToBean.parse(beanStrategy, reader);
    }

    public List<Transaction> getTransactionsFromXML(File file) throws Exception {

        JAXBContext jaxbContext = JAXBContext.newInstance(Record.class);

        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        Record rootRecord= (Record) jaxbUnmarshaller.unmarshal(file);

        return rootRecord.getTransactions();
    }

}
