package com.rabobank.assignment.service;

import com.rabobank.assignment.model.Transaction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ValidatorServiceImpll implements ValidatorService {

    @Override
    public List<Transaction> getDuplicateTransactionReference(List<Transaction> transactions) {
        Map<Long, Transaction> uniqueTransaction = new HashMap<>();
        List<Transaction> duplicateTransaction = new ArrayList<>();
        for (Transaction transaction : transactions) {
            if (uniqueTransaction.containsKey(transaction.getReference())) {
                duplicateTransaction.add(transaction);
            } else {
                uniqueTransaction.put(transaction.getReference(), transaction);
            }
        }
        List<Transaction> finalDuplicateRecords = new ArrayList<Transaction>();
        finalDuplicateRecords.addAll(duplicateTransaction);
        for (Transaction transaction : duplicateTransaction) {
            if (null != uniqueTransaction.get(transaction.getReference())) {
                finalDuplicateRecords.add(uniqueTransaction.get(transaction.getReference()));
                uniqueTransaction.remove(transaction.getReference());
            }
        }
        return finalDuplicateRecords;
    }

    @Override
    public List<Transaction> getEndBalanceErrorTransactions(List<Transaction> transactions) {
        List<Transaction> endBalanceErrorRecords = new ArrayList<>();
        for (Transaction transaction : transactions) {
            if (transaction.getEndBalance().compareTo(transaction.getStartBalance().add(transaction.getMutation())) != 0) {
                endBalanceErrorRecords.add(transaction);
            }
        }
        return endBalanceErrorRecords;
    }

}
