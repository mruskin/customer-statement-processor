package com.rabobank.assignment.service;

import com.rabobank.assignment.model.Transaction;

import java.util.List;

public interface ValidatorService {

    public List<Transaction> getDuplicateTransactionReference(List<Transaction> records);

    public List<Transaction> getEndBalanceErrorTransactions(List<Transaction> records);
}


