package com.rabobank.assignment.integration;

import com.rabobank.assignment.config.FileParser;
import com.rabobank.assignment.service.ValidatorService;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CustomerStatementControllerIntegrationTest {

    @LocalServerPort
    private int port;

    @MockBean
    ValidatorService validatorService;

    @MockBean
    FileParser fileParser;

    @Before
    public void setUp() throws Exception {
        RestAssured.port = port;
    }

    @Test
    public void testCsvUniqueTransactionSuccess() {
        RequestSpecification request = RestAssured.given().multiPart("file", new File("src/test/resources/data/records.csv"));
        request.post("/api/processStatement")
                .then()
                .assertThat()
                .body("transaction", CoreMatchers.equalTo(null))
                .body("apiError.status", CoreMatchers.equalTo("OK"));
    }

    @Test
    public void testXmlUniqueTransactionSuccess() {
        RequestSpecification request = RestAssured.given().multiPart("file", new File("src/test/resources/data/records.xml"));
         request.post("/api/processStatement")
                .then()
                .assertThat()
                .body("transaction", CoreMatchers.equalTo(null))
                .body("apiError.status", CoreMatchers.equalTo("OK"));
    }

}
