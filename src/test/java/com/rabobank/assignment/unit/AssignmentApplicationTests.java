package com.rabobank.assignment.unit;

import com.rabobank.assignment.config.FileParser;
import com.rabobank.assignment.model.Transaction;
import com.rabobank.assignment.service.ValidatorService;
import com.rabobank.assignment.service.ValidatorServiceImpll;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class AssignmentApplicationTests {

	@MockBean
	ValidatorService validatorService;

	@MockBean
	FileParser fileParser;

	@Test
	public void testUniqueTransactionsFail() {
		List<Transaction> inputList = Arrays.asList(
				new Transaction(123, "NL65INGB2095310012", "Food", new BigDecimal(2), new BigDecimal(1) , new BigDecimal(1)),
				new Transaction(123, "NL41RABO6864399966", "Football training", new BigDecimal(2), new BigDecimal(1), new BigDecimal(1)));
		ValidatorService validatorServiceImpl = new ValidatorServiceImpll();
		List<Transaction> duplicateRecords = validatorServiceImpl.getDuplicateTransactionReference(inputList);
		Assert.assertEquals(inputList.size(), duplicateRecords.size());
	}

	@Test
	public void testUniqueTransactionsSuccess() {
		List<Transaction> inputList = Arrays.asList(
				new Transaction(123, "NL65INGB2095310012", "Food", new BigDecimal(2), new BigDecimal(1) , new BigDecimal(1)),
				new Transaction(1234, "NL41RABO6864399966", "Football training", new BigDecimal(2), new BigDecimal(1), new BigDecimal(1)));
		ValidatorService validatorServiceImpl = new ValidatorServiceImpll();
		List<Transaction> duplicateRecords = validatorServiceImpl.getDuplicateTransactionReference(inputList);
		Assert.assertEquals(0, duplicateRecords.size());
	}

    @Test
    public void testTransactionBalanseFail() {
        List<Transaction> inputList = Arrays.asList(
                new Transaction(123, "NL65INGB2095310012", "Food", new BigDecimal(2), new BigDecimal(1) , new BigDecimal(1)),
                new Transaction(1234, "NL41RABO6864399966", "Football training", new BigDecimal(2), new BigDecimal(1), new BigDecimal(1)));
        ValidatorService validatorServiceImpl = new ValidatorServiceImpll();
        List<Transaction> endBalanceErrorRecords = validatorServiceImpl.getEndBalanceErrorTransactions(inputList);
        Assert.assertEquals(inputList.size(), endBalanceErrorRecords.size());
    }

    @Test
    public void testTransactionBalanseSuccess() {
        List<Transaction> inputList = Arrays.asList(
                new Transaction(123, "NL65INGB2095310012", "Food", new BigDecimal(3), new BigDecimal(1) , new BigDecimal(4)),
                new Transaction(1234, "NL41RABO6864399966", "Football training", new BigDecimal(5), new BigDecimal(-2), new BigDecimal(3)));
        ValidatorService validatorServiceImpl = new ValidatorServiceImpll();
        List<Transaction> endBalanceErrorRecords = validatorServiceImpl.getEndBalanceErrorTransactions(inputList);
        Assert.assertEquals(0, endBalanceErrorRecords.size());
    }

}
